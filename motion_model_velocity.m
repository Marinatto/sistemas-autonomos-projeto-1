% Algorithm for the Velocity Model Motion 
function likelihood = motion_model_velocity(xt, x_t0, u, dt, alpha)

    distributions = algorithm_distributions;
    x = xt(1);
    y = xt(2);
    theta = xt(3);
    
    last_x = x_t0(1);
    last_y = x_t0(2);
    last_theta = x_t0(3);
    
    v = u(1);
    w = u(2);

    alpha_1 = alpha(1);
    alpha_2 = alpha(2);
    alpha_3 = alpha(3);
    alpha_4 = alpha(4);
    alpha_5 = alpha(5);
    alpha_6 = alpha(6);

    mi = 0.5*( (last_x - x)*cos(last_theta) + (last_y - y)*sin(last_theta)) ...
        / ((last_y - y)*cos(last_theta) - (last_x - x)*sin(last_theta));
    
    x_c = (last_x + x)/2 + mi*(last_y - y);
    y_c = (last_y + y)/2 + mi*(x - last_x);
    
    r = sqrt((last_x-x_c)^2 + (last_y-y_c)^2);
    
    delta_theta = atan2(y-y_c, x-x_c) - atan2(last_y-y_c, last_x-x_c);
    
    w_hat = delta_theta/dt;
    v_hat = w_hat*r;
    gamma_hat = (theta-last_theta)/dt - w_hat;
    
    likelihood = distributions.prob_normal_distribution(v-v_hat, 0, alpha_1*v^2 + alpha_2*w^2)* ...
        distributions.prob_normal_distribution(w-w_hat, 0, alpha_3*v^2 + alpha_4*w^2)* ...
        distributions.prob_normal_distribution(gamma_hat, 0, alpha_5*v^2 + alpha_6*w^2);
end