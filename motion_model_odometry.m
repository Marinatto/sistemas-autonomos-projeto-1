function likelihood = motion_model_odometry(state_odom, state_0_odom, x_hypo, x0_hypo, alpha)

    distribution = algorithm_distributions;

    x_odom = state_odom(1); % xbarra_linha
    y_odom = state_odom(2); % ybarra_linha
    theta_odom = state_odom(3); %thetabarra_linha

    last_x_odom = state_0_odom(1); % xbarra
    last_y_odom = state_0_odom(2); % ybarra
    last_theta_odom = state_0_odom(3); %thetabarra

    x = x_hypo(1); % x_linha
    y = x_hypo(2); % y_linha
    theta = x_hypo(3); % theta_linha

    last_x = x0_hypo(1); 
    last_y = x0_hypo(2);
    last_theta = x0_hypo(3);

    alpha_1 = alpha(1);
    alpha_2 = alpha(2);
    alpha_3 = alpha(3);
    alpha_4 = alpha(4);  
    alpha_5 = alpha(5);
    alpha_6 = alpha(6);

    rot1 = atan2(y_odom - last_y_odom, x_odom - last_x_odom) - last_theta_odom;
    trans = sqrt((last_x_odom - x_odom)^2 + (last_y_odom - y_odom)^2);
    rot2 = theta_odom - last_theta_odom - rot1;

    rot1_hat = atan2(y - last_y, x - last_x) - last_theta;
    trans_hat = sqrt((last_x - x)^2 + (last_y - y)^2);
    rot2_hat = theta - last_theta - rot1_hat;

    dif_rot1 = (rot1-rot1_hat + pi) - 2*pi * floor((rot1-rot1_hat + pi) / (2*pi)) - pi;
    dif_rot2 = (rot2-rot2_hat + pi) - 2*pi * floor((rot2-rot2_hat + pi) / (2*pi)) - pi;

    likelihood = distribution.prob_normal_distribution(dif_rot1, 0, alpha_1*abs(rot1_hat) + alpha_2*trans_hat)* ...
                distribution.prob_normal_distribution(trans-trans_hat, 0, alpha_3*abs(rot1_hat) + alpha_4*(abs(rot1_hat)+abs(rot2_hat)))* ...
                distribution.prob_normal_distribution(dif_rot2, 0, alpha_5*abs(rot2_hat) + alpha_6*trans_hat);
end
