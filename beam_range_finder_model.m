function q = beam_range_finder_model(Z, x, m, z_param, max_range, z_star, sigma, lambda)
    distribution = algorithm_distributions;

    q = 1;

    z_hit = z_param(1);
    z_short = z_param(2);
    z_max = z_param(3);
    z_rand = z_param(4);

    for i = 1:length(Z)
        p = z_hit*distribution.prob_normal_distribution(Z(i), z_star(i), sigma) + ...
            z_short*distribution.prob_exp_distribution(Z(i), z_star(i), lambda) + ...
            z_max*distribution.prob_impulse_distribution(Z(i), max_range) + ...
            z_rand*distribution.prob_uniform_distribution(max_range);

        q = q*p;
    end
end
