% Plotting all data for Motion Models project

%% Plotting  distributions curves for motion model velocity algorithm
distributions = algorithm_distributions;

var = 0.1;
mean = 0;
a = -10:0.1:10;
n_dist = [];
t_dist = [];
e_dist = [];
u_dist = [];

for i=1:length(a)
    n_dist(i) = distributions.prob_normal_distribution(a(i), mean, var);
    t_dist(i) = distributions.prob_triangular_distribution(a(i), mean, var);
    e_dist(i) = distributions.prob_exp_distribution(mean, var, a(i));
    u_dist(i) = distributions.prob_uniform_distribution(length(a));
end

figure(1)
plot(a, n_dist)
title("Normal distribution")
figure(2)
plot(a, t_dist)
title("Triangular distribution")
figure(3)
plot(a, e_dist)
title("Exponential distribution")
figure(4)
plot(a,u_dist)
title("Uniform distribution")

%% Plotting distributions curves for sample model velocity algorithm
sample_dist = sample_algorithm_distributions;

var = 0.01;

n = 100000;

normal = zeros(n,1);
triang = zeros(n,1);

for i=1:n
    normal(i) = sample_dist.sample_normal_distribution(var);
    triang(i) = sample_dist.sample_triangular_distribution(var);
end

figure(3)
histogram(normal)
title("Normal distribution")

figure(4)
histogram(triang)
title("Triangular distribution")

%% Plotting motion model velocity likelihood for a specific condition

x = [0.0, 2.5, 1.57];
x0 = [0.0, 2.0, 1.57];
u = [5 0.0];
dt = 0.1;
alpha = [0.1, 0.1, 0.1, 0.1, 0.1, 0.1];

prob = motion_model_velocity(x, x0, u, dt, alpha);
disp(prob)

%% Plotting sample model velocity for some initial condition 

x0 = [0.0 2.0 1.57]';
u = [4 -2]';
dt = 0.1;
alpha = [0.01 0.001 0.01 0.001 0.001 0.001]';

n_samples = 100;

x = [];
y = [];
theta = [];

for i = 1:n_samples
    states = sample_model_velocity(x0, u, dt, alpha);
    x(end+1) = states(1);
    y(end+1) = states(2);
    theta(end+1) = states(3);
end
mean_x = mean(x);
mean_y = mean(y);

new_states = [x;y;theta];

x_t2 = [];
y_t2 = [];
theta_t2 = [];

 for n = 1:length(new_states(1,:))
     for i = 1:n_samples
         states_t2 = sample_model_velocity(new_states(:,n), u, dt, alpha);
         x_t2(end+1) = states_t2(1);
         y_t2(end+1) = states_t2(2);
         theta_t2(end+1) = states_t2(3);
     end
 end
 
 mean_x_t2 = mean(x_t2);
 mean_y_t2 = mean(y_t2);

plot(x,y,'o')
hold on
%plot([x0(1) mean_x], [x0(2) mean_y],'LineWidth', 2)
plot(x_t2,y_t2,'ko')
plot(x0(1), x0(2), 'ro','LineWidth', 1.5)
plot(mean_x, mean_y, 'go','LineWidth', 2)
plot(mean_x_t2, mean_y_t2, 'go','LineWidth', 2)
legend("X(t=1)", "X(t=2)", "Estado inicial")
xlabel("x");
ylabel("y"); grid on
hold off
% axis([0.99 1.01 0.99 1.01])
%% Plotting motion model odometry for a specific condition

x_hypo = [1.6, 1.9, 1.57];
x0_hypo = [.9, .97, 1.557];
odom = [1.55, 1.99, 1.67];
odom0 = [.91, .94, 1.54];
alpha = [0.1, 0.1, 0.1, 0.1, 0.1, 0.1];

prob_odom = motion_model_odometry(odom, odom0, x_hypo, x0_hypo, alpha);

disp(prob_odom)

%% Plotting sample model odometry for a specific condition

odom = [0.91, .94, 1.54, 1.55, 1.99, 1.67]; %last_state(3) + current_state(3)
x0 = [.91, .94, 1.53];
alpha = [0.01, 0.01, 0.01, 0.01, 0.001, 0.001];

n_samples = 100;

x_t1 = [];
y_t1 = [];
theta_t1 = [];

for i = 1:n_samples
    states = sample_model_odometry(odom, x0, alpha);
    x_t1(end+1) = states(1);
    y_t1(end+1) = states(2);
    theta_t1(end+1) = states(3);
end

mean_x_odom = mean(x_t1);
mean_y_odom = mean(y_t1);

states_t1 = [x_t1; y_t1; theta_t1];

x_t2 = [];
y_t2 = [];
theta_t2 = [];

new_odom = [odom(4), odom(5), odom(6), 4.32, 4.30, 0.0];

 for n = 1:length(states_t1(1,:))
     for i = 1:n_samples
         states_t2 = sample_model_odometry(new_odom, states_t1(:,n), alpha);
         x_t2(end+1) = states_t2(1);
         y_t2(end+1) = states_t2(2);
         theta_t2(end+1) = states_t2(3);
     end
 end
 
 mean_x_odom_t2 = mean(x_t2);
 mean_y_odom_t2 = mean(y_t2);

plot(x_t1,y_t1,'o')
hold on
plot(x_t2,y_t2,'ko')
plot(x0(1),x0(2),'ro')
%plot([x0(1) mean_x_odom], [x0(2) mean_y_odom],'LineWidth', 2)
plot(mean_x_odom, mean_y_odom, 'go', 'LineWidth', 2)
plot(mean_x_odom_t2, mean_y_odom_t2, 'go', 'LineWidth', 2)
legend("X(t=1)", "X(t=2)","Estado inicial")
xlabel("x")
ylabel("y")
grid on
hold off