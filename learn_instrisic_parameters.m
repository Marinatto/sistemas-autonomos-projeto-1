function Theta = learn_instrisic_parameters(Z, z_star, max_z, x, m)
    distributions = algorithm_distributions;

    sigma = 1.;
    lambda = 0.5;

    e_hit = [];
    e_short = [];
    e_max = [];
    e_rand = [];

    for k = 1:12
        for i = length(Z)
            normalizer = 1/(distributions.prob_normal_distribution(Z(i), z_star(i), sigma) + ...
                            distributions.prob_exp_distribution(Z(i), z_star(i), lambda) + ...
                            distributions.prob_impulse_distribution(Z(i), max(Z)) + ...
                            distributions.prob_uniform_distribution(length(Z)));
            e_hit(i) = normalizer*distributions.prob_normal_distribution(Z(i), z_star(i), sigma);
            e_short(i) = normalizer*distributions.prob_exp_distribution(Z(i), z_star(i), lambda);
            e_max(i) = normalizer*distributions.prob_impulse_distribution(Z(i), max_z);
            e_rand(i) = normalizer*distributions.prob_uniform_distribution(max_z);
        end

        Z_norm_inv = 1/norm(Z);
        z_hit = Z_norm_inv*sum(e_hit);
        z_short = Z_norm_inv*sum(e_short);
        z_max = Z_norm_inv*sum(e_max);
        z_rand = Z_norm_inv*sum(e_rand);

        sigma = sqrt((1/sum(e_hit)).*sum(e_hit.*(Z-z_star).^2));
        lambda = sum(e_short)/sum(e_short.*Z);

        Theta = [z_hit, z_short, z_max, z_rand, sigma, lambda]; 

    end
end