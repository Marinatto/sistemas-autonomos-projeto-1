% Algoritmos Sample

classdef sample_algorithm_distributions
    methods
        function y = sample_normal_distribution(self, var)
    
            b = sqrt(var);
            y = sum(2*b*rand(12,1) - b)/2;
    
        end

        function y = sample_triangular_distribution(self, var)
    
            b = sqrt(var);
            y = sqrt(6)*((2*b*rand(1,1) - b)+(2*b*rand(1,1) - b))/2;

        end
    end
end