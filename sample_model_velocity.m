function cur_state = sample_model_velocity(xt, u, dt, alpha)
    distributions = sample_algorithm_distributions;

    x = xt(1);
    y = xt(2);
    theta = xt(3);

    v = u(1);
    w = u(2);
    
    alpha_1 = alpha(1);
    alpha_2 = alpha(2);
    alpha_3 = alpha(3);
    alpha_4 = alpha(4);
    alpha_5 = alpha(5);
    alpha_6 = alpha(6);

    v_hat = v + distributions.sample_normal_distribution(alpha_1*v^2 + alpha_2*w^2);
    w_hat = w + distributions.sample_normal_distribution(alpha_3*v^2 + alpha_4*w^2);
    gamma_hat = distributions.sample_normal_distribution(alpha_5*v^2 + alpha_6*w^2);

    r = v_hat/w_hat;
    theta_hat = theta + w_hat*dt;
    
    cur_x = x - r*(sin(theta) - sin(theta_hat));
    cur_y = y + r*(cos(theta) - cos(theta_hat));
    cur_theta = theta_hat + gamma_hat*dt;

    cur_state = [cur_x, cur_y, cur_theta]';

end
