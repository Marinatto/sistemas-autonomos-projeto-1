function cur_state_sample = sample_model_odometry(u, x_t0, alpha)
    
    distributions = sample_algorithm_distributions;

    last_x_odom = u(1);
    last_y_odom = u(2);
    last_theta_odom = u(3);
    x_odom = u(4);
    y_odom = u(5);
    theta_odom = u(6);
    
    last_x = x_t0(1);
    last_y = x_t0(2);
    last_theta = x_t0(3);

    alpha_1 = alpha(1);
    alpha_2 = alpha(2);
    alpha_3 = alpha(3);
    alpha_4 = alpha(4);  
    alpha_5 = alpha(5);
    alpha_6 = alpha(6);

    rot1 = atan2(y_odom - last_y_odom, x_odom - last_x_odom) - last_theta_odom;
    trans = sqrt((x_odom - last_x_odom)^2 + (y_odom - last_y_odom)^2);
    rot2 = theta_odom - last_theta_odom - rot1;

    rot1_hat = rot1 - distributions.sample_normal_distribution(alpha_1*rot1^2 + alpha_2*trans^2);
    trans_hat = trans - distributions.sample_normal_distribution(alpha_3*trans^2 + alpha_4*rot1^2 + alpha_4*rot2^2);
    rot2_hat = rot2 - distributions.sample_normal_distribution(alpha_5*rot2^2 + alpha_6*trans^2);

    cur_x = last_x + trans_hat*cos(last_theta + rot1_hat);
    cur_y = last_y + trans_hat*sin(last_theta + rot1_hat);
    cur_theta = last_theta + rot1_hat + rot2_hat;

    cur_state_sample = [cur_x cur_y cur_theta]';

end