% Plotting all data for Robot Perception project

%% Plotting distributions curves used throughout the algorithms
distributions = algorithm_distributions;

z_max = 10.;
dt = 0.01;
z = 0:dt:z_max;
z_star = 5.;
sigma = 1.;
lambda = 0.5;

p_hit = [];
p_short = [];
p_max = [];
p_rand = [];

for i=1:length(z)
    p_hit(i) = distributions.prob_normal_distribution(z(i), z_star, sigma);
    p_short(i) = distributions.prob_exp_distribution(z(i), z_star, lambda);
    p_max(i) = distributions.prob_impulse_distribution(z(i), z_max);
    p_rand(i) = distributions.prob_uniform_distribution(z_max);
end

figure(1)
plot(z, p_hit)
title("P_{hit}")

figure(2)
plot(z, p_short)
title("P_{short}")

figure(3)
plot(z, p_max)
title("P_{max}")

figure(4)
plot(z, p_rand)
title("P_{rand}")

% Linear combination that characterize the sensor model

z_hit = 0.6;
z_short = 0.2;
z_max = 0.1;
z_rand = 0.1;

p = z_hit*p_hit + z_short*p_short + z_max*p_max + z_rand*p_rand;

figure(5)
plot(z, p)
title("Probability density function of the sensor model")
grid on
axis([0 10.5 0 0.4])

%% Plotting
grid_map = [0, 0, 1, 0, 0, 0;
        0, 0, 1, 0, 0, 0;
        0, 0, 0, 0, 1, 0;
        0, 0, 1, 1, 1, 0;
        0, 0, 0, 0, 1, 0];
map = occupancyMap(grid_map, 1);

x0 = [0.5, 4.5, 0];
max_range = 3.;

angles = linspace(-pi/2,pi/2,180); %-90~90 degrees, 1 degree resolution
z_star = [];

intersectionPts = rayIntersection(map, x0, angles, max_range);
ind = ~isnan(intersectionPts);

angles_hit = [];

for i = 1:length(angles)
    if ind(i,:) == [1 1]
        angles_hit(end+1) = angles(i);
    end
end

intersectionPts(isnan(intersectionPts)) = []; %Removing NaNs
intersectionPts = reshape(intersectionPts,[length(intersectionPts)/2,2]); %Fixing size of matrix: N by 2

for i = 1:length(intersectionPts) 
    z_star(end+1) = sqrt((intersectionPts(i,1)-x0(1))^2+(intersectionPts(i,2)-x0(2))^2);
end

Z = z_star + randn(1,length(z_star));

Theta = learn_instrisic_parameters(Z, z_star, max_range, x0, map);

z_hit = Theta(1);
z_short = Theta(2);
z_max = Theta(3);
z_rand = Theta(4);

z_param = [z_hit, z_short, z_max, z_rand];
sigma = Theta(5);
lambda = Theta(6);

q = beam_range_finder_model(Z, x0, map, z_param, max_range, z_star, sigma, lambda);
