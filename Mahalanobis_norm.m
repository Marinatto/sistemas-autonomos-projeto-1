% Maharalanobis distance

% m: Valor médio
% S: Matriz de variância
% n: Norma de Mahalanobis

elps = [];

m = [5;5];
S = [1 0.5; 0.5 1];
n = [1;2;3];

for i = 1:length(n)
    elps = [elps, mahalanobis_distance(m, S, n(i))];
end
mahalanobis_plot(elps);

function y = mahalanobis_distance(m, S, n)
    [F, D] = eig(S);
    
    theta = linspace(0,2*pi, 360)';

    y = n*[cos(theta), sin(theta)]*(D.^0.5)*F' + m';
end

function mahalanobis_plot(elps)

ns = size(elps,2)/2;

hold on
for i = 1:ns
    plot(elps(:,2*i-1), elps(:,2*i), 'LineWidth',2);
end  

end

