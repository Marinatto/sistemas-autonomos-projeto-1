% Algorithms for computing densities of a normal distribution and
% the triangular distribution with variance b.

classdef algorithm_distributions
    methods
        function y = prob_normal_distribution(self, z_k, mean, var)
            y = exp(-((z_k-mean)^2)/(2*var))/(sqrt(2*pi*var));
        end

        function y = prob_triangular_distribution(self, z_k, mean, var)
            vec = [0 1/(sqrt(6*var))-(abs(z_k-mean)/(6*var))];
            y = max(vec);
        end
        function y = prob_exp_distribution(self, z_k, mean, var)
            normalizer = 1/(1-exp(-var*mean));
            if z_k >= 0 && z_k <= mean
                y =  normalizer*var*exp(-var*z_k);
            else
                y = 0;
            end
        end
        function y = prob_uniform_distribution(self, z_max)
            y = 1/z_max;
        end
        function y = prob_impulse_distribution(self, z_k, z_max)
            if z_k == z_max
                y = 1;
            else
                y = 0;
            end
        end
    end
end